<!-- Navigation -->
<div class="<?= ((isset($page) && $page != 'home')) ? 'secpage shadow-off bgwhite offsetTrue' : 'navigation' ?> ">
    <div class="container">
        <div class="logo"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'index', 'prefix' => false]); ?>">
                <?= $this->Html->image('../images/spacer.png', ['alt' => 'ignis', 'class' => 'mt5 relative z100']); ?>
            </a>
        </div>

        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid relative">

                <button type="button" class="btn left hide-show-button none">
                    <span class="burgerbar"></span>
                    <span class="burgerbar"></span>
                    <span class="burgerbar"></span>
                </button>
                <a href="#" class="closemenu"></a> 

                <!-- mobile version drop menu -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hamburger" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- menu -->
                <div class="mainmenu mobanim dark-menu navbar-collapse white collapse offset-0 ">
                    <ul class="nav navbar-nav mobpad">

                        <li class="dropdown">
                            <a class="dropdown-toggle" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'index', 'prefix' => false]); ?>">Accueil <b class="caret"></b></a>                            
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'presentation']); ?>">Vidéos <b class="caret"></b></a>                    
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#contact">Contact <b class="caret"></b></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="http://www.ignis-communication.com/" target="_blank">Ignis <b class="caret"></b></a>
                        </li>
                    </ul>
                </div>

                <div class="dots">
                    <a href="#" class="opendots">
                        <span class="icon-dots"></span>
                        <span class="icon-dots"></span>
                        <span class="icon-dots"></span>
                    </a>
                </div>			

            </div>
        </div>		

    </div>
</div>
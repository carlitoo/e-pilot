<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Message du site epilote.ignis.fr</title>
        <style type="text/css">
            <!--
            body {
                background-color: #eeeeee;
                font-family: Arial, Helvetica, sans-serif;
            }
            td {
                font-size: 12px;
            }
            td h3 {
                font-weight: bold;
                font-size: 16px;
                text-transform: uppercase;
                margin: 0;
                color: #65ae48;
            }
            td h3 span {
                color: #333333;
            }
            td h4 {
                font-size: 20px;
                text-transform: uppercase;
                color: #0A131E;
                margin: 0;
            }
            .value {
                font-size: 14px;
            }
            -->
        </style>
    </head>

    <body bgcolor="#eeeeee">
        <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
            <tr>
                <td colspan="4" height="100" valign="center" align="center" bgcolor="#333333">
                    <a href="http://www.reseau-proeco-energies.fr" style="color: #ffffff;">
                        <?= $this->Html->image('logo-epilote.png', [
                            'alt' => "epilote.ignis.fr",
                            'width' => 200,
                            'border' => 0,
                            'fullBase' => true
                        ]); ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td width="15">&nbsp;</td>
                <td width="170">&nbsp;</td>
                <td height="80" width="400" align="left" valign="center">
                    <h3>Nouveau message <br /><span>Sujet : <?= $datas['title']; ?></span></h3>
                </td>
                <td width="15">&nbsp;</td>
            </tr>
            <?= $this->fetch('content'); ?>
            <tr>
                <td colspan="4">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" height="10" style="background-color: #F7BF5B; font-size: 1px;">&nbsp;</td>
                            <td width="200" height="10" style="background-color: #00639D; font-size: 1px;">&nbsp;</td>
                            <td width="200" height="10" style="background-color: #F7BF5B; font-size: 1px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgcolor="#333333">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td height="60" valign="center" align="left">
                    <p style="color: #ffffff; font-size: 11px; line-height: 17px;">
                        La demande a eu lieu le <strong><?= date('d/m/Y') ?></strong> à <strong><?= date('H:i') ?></strong><br>
                        <span style="color: #ffffff;">Message automatique du site : </span><br>
                        <a href="https://epilote.ignis.fr" style="color: #ffffff;">epilote.ignis.fr</a>
                    </p>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
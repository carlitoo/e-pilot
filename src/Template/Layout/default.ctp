<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>E-pilote, logiciel de gestion de la relation commerciale. CRM sur mesure</title>
        <?= $this->Html->meta('icon') ?>

        <!--CSS-->
        <?= $this->Html->css(['bootstrap', 'style', 'extralayers', 'plugins/rs-plugin/css/settings', 'caroufredsel', 'animate', 'jquery.easy-pie-chart', 'HoverEffectIdeas/css/demo', 'HoverEffectIdeas/css/set1', 'plugins/lightcase/css/lightcase', 'custom']); ?>
        <!--FONT-->
        <?= $this->Html->css(['../fonts/fontello/css/fontello', '../fonts/fontello/css/animation', '../fonts/dripicons/webfont', '../fonts/simple-line-icons/simple-line-icons', '../fonts/themify-icons/themify-icons', '../fonts/fontastic/styles']); ?>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css' />
        <!-- GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- ANONYMOUS PRO FONT-->
        <link href="http://fonts.googleapis.com/css?family=Anonymous+Pro:400,700" rel="stylesheet" type="text/css" />        

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="removebullets">
        <!-- start preloader -->
        <!--        <div id="preloader">
                    <div id="status">&nbsp;</div>
                </div>-->
        <!-- menu -->
        <?= $this->element('menu'); ?>

        <?= $this->Flash->render() ?>
        <?= $this->Html->script(['jquery']); ?>
        <!--<div class="container clearfix">-->
        <?= $this->fetch('content') ?>
        <!--</div>-->

        <footer>
            <!-- FOOTER -->
            <div class="footer white">

                <div class="clearfix"></div>
                <div class="separator100dark"></div>

                <div class="container ptb30 cwhite" style="text-align: center">
                    <span style="color: #ccc">©Ignis communication | </span>
                    <!--<a class="cgv"href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'cgv']); ?>">Conditions générales de vente</a>-->
                    <!--<a href="#"><img src="images/avision-footerb.png" class="w90" alt="avision logo"></a>-->
                    <!--                    <div class="socialicons right">
                                            <ul>
                                                <li class="blue dgrey"><a href="#"><i class="icon-facebook"></i></a></li>
                                                <li class="lblue lgrey"><a href="#"><i class="icon-twitter-bird"></i></a></li>
                                                <li class="orange dgrey"><a href="#"><i class="icon-gplus"></i></a></li>
                                                <li class="pink lgrey"><a href="#"><i class="icon-dribbble"></i></a></li>
                                                <li class="red dgrey"><a href="#"><i class="icon-youtube"></i></a></li>
                                            </ul>
                                        </div>-->
                </div>
            </div>
            <!-- END OF FOOTER -->
        </footer>
        <!--JS-->
        <?= $this->Html->script(['compressed', 'animations', 'functions', 'lib/dist/js/bootstrap.min']); ?>
        <?= $this->Html->script(['jquery.mixitup.min']) ?>
        <?= $this->fetch('script') ?>
    </body>
</html>

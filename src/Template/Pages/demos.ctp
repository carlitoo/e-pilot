<div class="mixpercent center owhidden mb100">
    <div id="mixItUp" class="">
        <div class="mix col3 category-3" data-my-order="1">
            <div class="mainwrap">
                <img src="images/tm16/gal01.jpg" class="fwi" alt="">
                <a class="mixhover red" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'demos1']); ?>">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Web Design and Development</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-2" data-my-order="2">
            <div class="mainwrap">
                <img src="images/tm16/gal02.jpg" class="fwi" alt="">
                <a class="mixhover lime" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Graphic Design</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-1" data-my-order="3">
            <div class="mainwrap">
                <img src="images/tm16/gal03.jpg" class="fwi" alt="">
                <a class="mixhover orange" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Corporate Identity</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-2" data-my-order="4">
            <div class="mainwrap">
                <img src="images/tm16/gal04.jpg" class="fwi" alt="">
                <a class="mixhover blue" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Graphic Design</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-1" data-my-order="5">
            <div class="mainwrap">
                <img src="images/tm16/gal05.jpg" class="fwi" alt="">
                <a class="mixhover turquoise" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Corporate Identity</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-3" data-my-order="6">
            <div class="mainwrap">
                <img src="images/tm16/gal06.jpg" class="fwi" alt="">
                <a class="mixhover purple" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Web Design and Development</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-3" data-my-order="7">
            <div class="mainwrap">
                <img src="images/tm16/gal07.jpg" class="fwi" alt="">
                <a class="mixhover black" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Web Design and Development</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-2" data-my-order="8">
            <div class="mainwrap">
                <img src="images/tm16/gal08.jpg" class="fwi" alt="">
                <a class="mixhover cealk" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Graphic Design</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="mix col3 category-1" data-my-order="9">
            <div class="mainwrap">
                <img src="images/tm16/gal09.jpg" class="fwi" alt="">
                <a class="mixhover red" href="#">
                    <div class="valign">
                        <h1>Project title</h1>
                        <p>Corporate Identity</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- SECTION CONTACT -->
<div class="" id="contact">
    <div class="container sspacing-title-button">
        <div class="row">
            <p class="text-center size30 mb10 pb20">Laissez-nous un message, nous vous rappelerons pour évoquer votre projet.</p>
            <?= $this->Flash->render(); ?>
            <?php echo $this->Form->create("Contact", ['id' => 'contact-form', 'type' => 'post']); ?>
            <div class="col-md-3">
                <input class="form-control formlarge" placeholder="Nom" name="nom" type="text">
                <input class="form-control formlarge mt17" placeholder="Email" name="email" type="text">
                <input class="form-control formlarge mt17" placeholder="Téléphone" name="telephone" type="text">
            </div>
            <div class="col-md-9">
                <textarea class="form-control formstyle" rows="7" name="message" placeholder="Message"></textarea>
            </div>
            <div class="clearfix"></div>
            <button style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-iteration-count: 2; animation-name: pulse;" type="submit" id="send-mail" class="btn btnwhitebig btn-default caps center mt30 wow pulse animated animated" data-wow-delay="0.4s" data-wow-duration="1s" data-wow-iteration="2"><i class="icon-mail"></i> Envoyer</button>
            <div class="clearfix"></div>
            <?php echo $this->Form->hidden("captcha"); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- END OF CONTACT -->
<!--<div class="bgwhite space20 relative z20"></div>-->

<!--<div class="container bgxlight bordertopbottom relative z20">
	<div class="container ptb40 cdark">
		<h3 class="text-center titlefont size30 c999">Contact</h3>
		<div class="pt10 breadcrumbs">
			<a href="#"><span class="ti-home"></span></a> <i>/</i>
			<a href="#">Contact</a>
		</div>
		 <div class="toparrow"></div> 
	</div>
</div>-->
<!-- GOOGLE MAPS -->
<div class="wfull h500 bgwhite relative z15 footer-bottom-offset">
	<div class="col-md-12 hfull bgdark offset-0">
		<div id="googleMap"></div>
	</div>
</div>
<!-- /END GOOGLE MAPS -->
<div class="">
    <div class="container sspacing-title-button">
        <div class="row">
            <p class="text-center size30 mb10 pb20">Laissez-nous un message, nous vous rappelerons pour évoquer votre projet.</p>
            <?= $this->Flash->render(); ?>
            <?php echo $this->Form->create("Contact", ['id' => 'contact-form', 'type' => 'post']); ?>
            <div class="col-md-3">
                <input class="form-control formlarge" placeholder="Nom" name="nom" type="text">
                <input class="form-control formlarge mt17" placeholder="Email" name="email" type="text">
                <input class="form-control formlarge mt17" placeholder="Téléphone" name="telephone" type="text">
            </div>
            <div class="col-md-9">
                <textarea class="form-control formstyle" rows="7" name="message" placeholder="Message"></textarea>
            </div>
            <div class="clearfix"></div>
            <button style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-iteration-count: 2; animation-name: pulse;" type="submit" id="send-mail" class="btn btnwhitebig btn-default caps center mt30 wow pulse animated animated" data-wow-delay="0.4s" data-wow-duration="1s" data-wow-iteration="2"><i class="icon-mail"></i> Envoyer</button>
            <div class="clearfix"></div>
            <?php echo $this->Form->hidden("captcha"); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- GOOGLE MAPS -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/googlemaps.js"></script>
<!-- START REVOLUTION SLIDER 3.1 rev5 fullwidth mode -->
<div class="tp-banner-container">
    <div class="e-pilote">
        <?= $this->Html->image('logo-epilote_1.png', ['alt' => 'ignis', 'class' => '']); ?>
    </div>
    <div class="tp-banner" >
        <ul>

            <!-- SLIDE 0  -->
            <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-color="black">
                <!-- MAIN IMAGE -->
                <img src="images/slider/bg-slide.jpg"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">

                <!-- LAYER 1 -->
                <div class="tp-caption caption-smallgrey customin customout tp-resizeme"
                     data-x="center"  data-voffset="-90"		
                     data-y="center"  data-hoffset="0"	

                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="500"
                     data-easing="Power3.easeInOut"
                     data-splitin="chars"
                     data-splitout="chars"
                     data-elementdelay="0.08"
                     data-endelementdelay="0.08"						
                     data-endspeed="300"
                     style="z-index: 10;color:#3BAC72;font-size: 22px;margin-top: 50px">Simple, Efficace, Performant
                </div>

                <!-- LAYER 2 -->
                <div class="tp-caption caption-smallgrey customin customout tp-resizeme"
                     data-x="center"  data-voffset="-40"		
                     data-y="center"  data-hoffset="0"	

                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1500"
                     data-easing="Power3.easeInOut"
                     data-splitin="chars"
                     data-splitout="chars"
                     data-elementdelay="0.08"
                     data-endelementdelay="0.08"						
                     data-endspeed="300"
                     style="z-index: 10;margin-top: 60px;font-family: Raleway">Logiciel de gestion de la relation commerciale
                </div>

                <!-- LAYER 3 -->
                <div class="tp-caption caption-dark-bigbold-caps customin customout tp-resizeme"
                     data-x="center"  data-voffset="-150"		
                     data-y="center"  data-hoffset="0"	

                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1500"
                     data-easing="Power3.easeInOut"
                     data-splitin="chars"
                     data-splitout="chars"
                     data-elementdelay="0.08"
                     data-endelementdelay="0.08"						
                     data-endspeed="300"
                     style="z-index: 10">
                </div>


                <!-- LAYER 4 -->
                <!--                				<div class="tp-caption caption-white-bold-caps  customin skewtoleft"
                                                                        data-x="center"   data-voffset="20"	
                                                                        data-y="center"   data-hoffset="-70"
                                                                
                                                                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                                        data-speed="500"
                                                                        data-start="3500"
                                                                        data-easing="Back.easeOut"
                                                                        data-endspeed="300"
                                                                        data-endeasing="Back.easeIn"><button type="submit" class="btn btndarkline2 caps center">Features</button>
                                                                </div>	-->

                <!-- LAYER 5 -->
                <!--				<div class="tp-caption caption-white-bold-caps customin skewtoleft"
                                                        data-x="center"   data-voffset="20"
                                                        data-y="center"   data-hoffset="70"	
                                                                                                        
                                                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                        data-speed="500"
                                                        data-start="4000"
                                                        data-easing="Back.easeInOut"
                                                        data-endspeed="300"
                                                        data-endeasing="Back.easeIn"><button type="submit" class="btn btnorange caps center">Buy now</button>
                                                </div>	-->

                <!-- LAYER 6 -->
                <div class="tp-caption customin customout"
                     data-x="center"  data-voffset="0"		
                     data-y="bottom"  data-hoffset="0"	

                     data-customin="x:0;y:0;z:0;rotationX:-90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 100%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="1000"
                     data-easing="Power3.easeInOut"
                     data-endspeed="500"
                     data-endeasing="Power3.easeInOut"					
                     style="z-index: 9"><img src="images/slider/desktop.png" alt=""/>
                </div>

                <!-- LAYER 7 -->
                <div class="tp-caption lfb"
                     data-x="center"  data-voffset="-30"		
                     data-y="bottom"  data-hoffset="-510"	

                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="1900"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Back.easeOut"					
                     style="z-index: 5"><img src="images/slider/pen_1.png" alt=""/>
                </div>			

                <!-- LAYER 8 -->
                <!--                <div class="tp-caption lfb"
                                     data-x="center"  data-voffset="290"		
                                     data-y="bottom"  data-hoffset="-700"	
                
                                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1500"
                                     data-easing="Back.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Back.easeOut"					
                                     style="z-index: 5"><img src="images/slider/note.png" alt=""/>
                                </div>	-->

                <!-- LAYER 9 -->
                <!--                <div class="tp-caption lfb"
                                     data-x="center"  data-voffset="120"		
                                     data-y="bottom"  data-hoffset="-700"	
                
                                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1700"
                                     data-easing="Back.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Back.easeOut"					
                                     style="z-index: 9"><img src="images/slider/wallet.png" alt=""/>
                                </div>	-->

                <!-- LAYER 10 -->
                <div class="tp-caption lfb"
                     data-x="center"  data-voffset="50"		
                     data-y="bottom"  data-hoffset="-750"	

                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="1900"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Back.easeOut"					
                     style="z-index: 9"><img src="images/slider/key.png" alt=""/>
                </div>		


                <!-- LAYER 11 -->
                <!--                <div class="tp-caption lfb"
                                     data-x="center"  data-voffset="80"		
                                     data-y="bottom"  data-hoffset="710"	
                
                                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="2100"
                                     data-easing="Back.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Back.easeOut"					
                                     style="z-index: 8"><img src="images/slider/tablet.png" alt=""/>
                                </div>		-->

                <!-- LAYER 12 -->
                <div class="tp-caption lfb"
                     data-x="center"  data-voffset="150"		
                     data-y="bottom"  data-hoffset="780"	

                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="2200"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Back.easeOut"					
                     style="z-index: 2"><img src="images/slider/tablette.png" alt=""/>
                </div>	

                <!-- LAYER 13 -->
                <div class="tp-caption lft"
                     data-x="center"  data-voffset="80"		
                     data-y="top"  data-hoffset="400"	

                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="2400"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Back.easeOut"					
                     style="z-index: 2"><img src="images/slider/coffee.png" alt=""/>
                </div>			

                <!-- LAYER 14 -->
                <div class="tp-caption lft"
                     data-x="center"  data-voffset="-100"		
                     data-y="top"  data-hoffset="700"	

                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="2600"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Back.easeOut"					
                     style="z-index: 2"><img src="images/slider/portable.png" alt=""/>
                </div>										
            </li>

            <!-- SLIDE 0  -->
            <!--            <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-color="white">
                             MAIN IMAGE 
                            <img src="images/slider/bg-slide1.jpg"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
            
                             LAYER 1 
                            <div class="tp-caption caption-smallgrey customin customout tp-resizeme"
                                 data-x="center"  data-voffset="-90"		
                                 data-y="center"  data-hoffset="0"	
            
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="500"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="chars"
                                 data-splitout="chars"
                                 data-elementdelay="0.08"
                                 data-endelementdelay="0.08"						
                                 data-endspeed="300"
                                 style="z-index: 10">Start bulding your website in minutes. Front end editing!
                            </div>
            
                             LAYER 2 
                            <div class="tp-caption caption-smallgrey customin customout tp-resizeme"
                                 data-x="center"  data-voffset="-40"		
                                 data-y="center"  data-hoffset="0"	
            
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1500"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="chars"
                                 data-splitout="chars"
                                 data-elementdelay="0.08"
                                 data-endelementdelay="0.08"						
                                 data-endspeed="300"
                                 style="z-index: 10">Best drag & drop framework ever! 
                            </div>
            
                             LAYER 3 
                            <div class="tp-caption caption-white-bigbold-caps customin customout tp-resizeme"
                                 data-x="center"  data-voffset="-150"		
                                 data-y="center"  data-hoffset="0"	
            
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1500"
                                 data-easing="Power3.easeInOut"
                                 data-splitin="chars"
                                 data-splitout="chars"
                                 data-elementdelay="0.08"
                                 data-endelementdelay="0.08"						
                                 data-endspeed="300"
                                 style="z-index: 10">Webdesign
                            </div>
            
            
                             LAYER 4 
                            <div class="tp-caption caption-white-bold-caps  customin skewtoleft"
                                 data-x="center"   data-voffset="20"	
                                 data-y="center"   data-hoffset="-70"
            
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="3500"
                                 data-easing="Back.easeOut"
                                 data-endspeed="300"
                                 data-endeasing="Back.easeIn"><button type="submit" class="btn btndarkround caps center">Features</button>
                            </div>	
            
                             LAYER 5 
                            <div class="tp-caption caption-white-bold-caps customin skewtoleft"
                                 data-x="center"   data-voffset="20"
                                 data-y="center"   data-hoffset="70"	
            
                                 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="4000"
                                 data-easing="Back.easeInOut"
                                 data-endspeed="300"
                                 data-endeasing="Back.easeIn"><button type="submit" class="btn btnorangeround caps center">Buy now</button>
                            </div>	
            
                             LAYER 6 
                            <div class="tp-caption customin customout"
                                 data-x="center"  data-voffset="0"		
                                 data-y="bottom"  data-hoffset="0"	
            
                                 data-customin="x:0;y:0;z:0;rotationX:-90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 100%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="1000"
                                 data-easing="Power3.easeInOut"
                                 data-endspeed="500"
                                 data-endeasing="Power3.easeInOut"					
                                 style="z-index: 9"><img src="images/slider/laptop.png" alt=""/>
                            </div>
            
                             LAYER 7 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="-30"		
                                 data-y="bottom"  data-hoffset="-510"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="1900"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 5"><img src="images/slider/pen.png" alt=""/>
                            </div>			
            
                             LAYER 8 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="290"		
                                 data-y="bottom"  data-hoffset="-700"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="1500"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 5"><img src="images/slider/note.png" alt=""/>
                            </div>	
            
                             LAYER 9 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="120"		
                                 data-y="bottom"  data-hoffset="-700"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="1700"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 9"><img src="images/slider/wallet.png" alt=""/>
                            </div>	
            
                             LAYER 10 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="250"		
                                 data-y="bottom"  data-hoffset="-700"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="1900"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 9"><img src="images/slider/iphone.png" alt=""/>
                            </div>		
            
            
                             LAYER 11 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="80"		
                                 data-y="bottom"  data-hoffset="710"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="2100"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 8"><img src="images/slider/tablet.png" alt=""/>
                            </div>		
            
                             LAYER 12 
                            <div class="tp-caption lfb"
                                 data-x="center"  data-voffset="80"		
                                 data-y="bottom"  data-hoffset="710"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="2200"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 9"><img src="images/slider/camera.png" alt=""/>
                            </div>	
            
                             LAYER 13 
                            <div class="tp-caption lft"
                                 data-x="center"  data-voffset="80"		
                                 data-y="top"  data-hoffset="400"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="2400"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 2"><img src="images/slider/coffee.png" alt=""/>
                            </div>			
            
                             LAYER 14 
                            <div class="tp-caption lft"
                                 data-x="center"  data-voffset="-100"		
                                 data-y="top"  data-hoffset="700"	
            
                                 data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700"
                                 data-start="2600"
                                 data-easing="Back.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Back.easeOut"					
                                 style="z-index: 2"><img src="images/slider/psvita.png" alt=""/>
                            </div>										
                        </li>-->
        </ul>
        <div class="tp-bannertimer none"></div>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<!-- SECTION SERVICES -->
<div class="">
    <div class="container sspacing">
        <div class="row">
            <ul class="services">
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <!--<i class="icon-star-2 size50 lh50 clightgrey"></i>-->
                        <i class="icon-thumbs-up lh50 size50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">Fonctionnalités intuitives</h4>
                        Fiche client, devis, suivis factures....<br>
                        les fonctions essentielles
                    </a>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <i class="ti-tablet size50 lh50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">Mobilité assurée</h4>
                        Mobile, il vous suit partout sur vos smartphones et tablettes...
                    </a>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <i class="ti-stats-up lh50 size50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">100% évolutif</h4>
                        E-pilote grandit avec votre entreprise et s'adapte à vos besoins métier
                    </a>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <i class="icon-user lh50 size50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">Accompagnement</h4>
                        Créez une dynamique collective dans l'entreprise.<br>
                        Formation prise en charge par votre OPCA
                    </a>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <i class="ti-lock lh50 size50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">Sécurisé</h4>
                        Simple et sûr :<br>
                        migration facile et sécurisation de vos données
                    </a>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-12 text-center">
                    <a>
                        <i class="ti-stats-down lh50 size50 clightgrey"></i>
                        <h4 class="uppercase titlefont mtb10 titlefont-custom">Économique</h4>
                        Allégez votre fiscalité : <br>
                        louez sans engagement
                    </a>
                </li>
            </ul>	
        </div>
    </div>
</div>
<!-- END OF SECTION SERVICES -->

<div class="bgwhite relative z10">
    <div class="container">
        <div class="row ">
            <!-- SECTION -->
            <div class="borderall">
                <div class="col-lg-6 p40">
                    <div class="clearfix"></div>
                    <h1 class="titlefont size40 titlefont-custom-green">Simple, efficace et performant.</h1>
                    <!-- <h2 class="fontproximalight size30 cmaincolor"></h2> -->
                    <div class="separator100 mtb30"></div>
                    <p class="size16 ">
                        Les développeurs de l'agence Ignis ont répondu à la demande de leurs clients et ont conçu un outil en ligne simple, efficace et performant.<br>E-pilote a pris le contrepied des offres du marché pour les logiciels de gestion commerciale : pas d'accessoires et d'options en nombre excessif.<br>
                        De la simplicité et les justes applications.<br><br>
                        Si vous ne voulez pas de fonctions superflues, E-pilote est pour vous.
                    </p>
                </div>
                <div class="col-lg-6 offset-0">
                    <div class="grid">
                        <figure class="effect-sadie2">
                            <img src="images/tm17/simple.jpg" alt="img14"/>
                            <figcaption>
                                <h2>Simplicité</h2>
<!--                                <p>Make a /<br/> request</p>
                                <a href="#">View more</a>-->
                            </figcaption>			
                        </figure>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- END OF SECTION  -->
            <!-- SECTION -->
            <div class="borderall mtb40">                
                <div class="col-lg-6 offset-0">
                    <div class="grid">
                        <figure class="effect-sadie2">
                            <img src="images/tm17/libre_de_licence.jpg" alt="img14"/>
                            <figcaption>
                                <h2>libre de licence</h2>
<!--                                <p>Make a /<br/> request</p>
                                <a href="#">View more</a>-->
                            </figcaption>			
                        </figure>
                    </div>
                </div>
                <div class="col-lg-6 p40">
                    <div class="clearfix"></div>
                    <h1 class="titlefont size40 titlefont-custom-green">Libre et sans investissement</h1>
                    <!-- <h2 class="fontproximalight size30 cmaincolor"></h2> -->
                    <div class="separator100 mtb30"></div>
                    <p class="size16 ">
                        E-pilote est un logiciel développé sur la base d'un langage "libre". Sa souplesse lui permet de s'adapter aux différents ERP et autres datas.<br><br>E-pilote n'impose pas de licences, il est en location, sans engagement.<br><br> Les développements spécifiques effectués pour s'adapter à votre juste besoin peuvent être repris par d'autres informaticiens maitrisant ce langage, si besoin
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SECTION -->
            <div class="borderall mtb40">
                <div class="col-lg-6 p40">
                    <div class="clearfix"></div>
                    <h1 class="titlefont size40 titlefont-custom-green">Efficace</h1>
                    <!-- <h2 class="fontproximalight size30 cmaincolor"></h2> -->
                    <div class="separator100 mtb30"></div>
                    <p class="size16 ">
                        E-pilote est un logiciel développé sur la base d'un langage "libre". Sa souplesse lui permet de s'adapter aux différents ERP et autres datas.<br><br>E-pilote n'impose pas de licences, il est en location, sans engagement.<br><br> Les développement spécifiques éffectués pour s'adapter à votre juste besoin peuvent être repris par d'autres informaticiens maitrisant ce langae, si besoin
                    </p>
                </div>
                <div class="col-lg-6 offset-0">
                    <div class="grid">
                        <figure class="effect-sadie2">
                            <img src="images/tm17/sans_superflu.jpg" alt="img14"/>
                            <figcaption>
                                <h2>Sans superflu</h2>
<!--                                <p>Make a /<br/> request</p>
                                <a href="#">View more</a>-->
                            </figcaption>			
                        </figure>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- SECTION  -->
            <div class="borderall mtb40">
                <div class="col-lg-6 offset-0">
                    <div class="grid">
                        <figure class="effect-sadie2">
                            <img src="images/tm17/evolutif.jpg" alt="img14"/>
                            <figcaption>
                                <h2>Évolution sur mesure</h2>
<!--                                <p>Make a /<br/> request</p>
                                <a href="#">View more</a>-->
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-6 p40">
                    <h1 class="titlefont size40 titlefont-custom-green">Évolutif, E-pilote s'adapte à votre métier.</h1>
                    <!-- <h2 class="fontproximalight size30 cmaincolor"></h2> -->
                    <div class="separator100 mtb30"></div>
                    <p class="size16 ">Vous êtes déjà équipé, mais l'outil que vous avez ne s'ajuste pas parfaitement à vos attentes pour gagner en productivité. E-pilote vous fera économiser du temps de l'argent et progresser en performance.<br><br> Vous cherchez, par exemple, une solution pour géolocaliser vos clients au plus près de vos équipes ?<br> Vous avez des contrats longs avec vos clients et vous avez besoin d'y associer de nombreux fichiers ?<br><br> Avec E-pilote, toutes vos spécificités deviennent plus productives.                        
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- END OF SECTION  -->
            

            <!-- SECTION  -->
            <div class="borderall mtb40">
                <div class="col-lg-6 p40">
                    <h1 class="titlefont size40 titlefont-custom-green">installation prise en charge</h1>
                    <!-- <h2 class="fontproximalight size30 cmaincolor"></h2> -->
                    <div class="separator100 mtb30"></div>
                    <p class="size16 ">
                        E-pilote vous forme vous et vos équipes à son utilisation.<br><br> Cette formation collective est le moyen de galvaniser votre équipe et de partager l'optimisation du travail de chacun pour plus de performance, d'économies et de compétitivité.<br><br> Séance de 2 fois 3h30 de formation à E-pilote et d'aide à la configuration sur votre métier (650 euros pris en charge par votre organisme collecteur des budgets formation).<br><br> L'organisme de formation E-pilote vous guidera auprès de votre OPCA (Organismes paritaires collecteurs agréés).
                    </p>
                </div>
                <div class="col-lg-6 offset-0">
                    <div class="grid">
                        <figure class="effect-sadie2">
                            <img src="images/tm17/galvaniser.jpg" alt="img14"/>
                            <figcaption>
                                <h2>Galvaniser l'équipe</h2>
<!--                                <p>Make a /<br/> request</p>
                                <a href="#">View more</a>-->
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="clearfix"></div>
                </div>                
                <div class="clearfix"></div>
            </div>         

        </div>                            
    </div>
</div>



<!-- PRICES -->
<section class="" style="background:#2DB573;padding: 2px;margin-top: 80px">
    <div class="container mtb80">
        <div class="row">
            <!--<h3 class="text-center caps pb20 titlefont">Prices</h3>-->
            <!-- PRICE TABLE -->
            <div class="pricelist-wrapper dark col5">
                <ul>
                    <li class="noselections">
                        <div class="plistrow0">Sign Up for</div>
                        <div class="plistrow2 plistprice" style="color: #2DB573 !important">Location</div>
                        <div class="plistrowbg">Nombre d'utilisateurs</div>
                        <div class="plistrowbg">Gestion clients/prospects</div>
                        <div class="plistrow">Devis/affaires/factures</div>
                        <div class="plistrowbg">Devis/factures par mail</div>
                        <div class="plistrow">Tableau de bord</div>
                        <div class="plistrowbg">Mises à jour</div>
                        <div class="plistrowbg">Hébergement</div>
                        <div class="plistrow"></div>
                        <div class="plistrow2"></div>
                    </li>
                    <li class="noselections">
                        <div class="plistrow1" style="color: #2DB573 !important">Small</div>
                        <div class="plistrow2"><span class="h9" style="color: #245C5D !important;font-size: 20px;">*25€</span><br><span class="smallp">/mois</span></div>
                        <div class="plistrowbg"style="font-size: 26px;">1</div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow2"><div class="button_ok"><a href="#contact">Je choisis</a></div></div>
                    </li>
                    <li class="plistselected">
                        <div class="plistrow1" style="color: #2DB573 !important">Basic</div>
                        <div class="plistrow2"><span class="h9" style="color: #245C5D !important;font-size: 20px;">*35€</span><br><span class="smallp">/mois</span></div>
                        <div class="plistrowbg"style="font-size: 26px;">5</div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow2"><div class="button_ok"><a href="#contact">Je choisis</a></div></div>
                    </li>
                    <li class="noselections">
                        <div class="plistrow1" style="color: #2DB573 !important">Middle</div>
                        <div class="plistrow2"><span class="h9" style="color: #245C5D !important;font-size: 20px;">*40€</span><br><span class="smallp">/mois</span></div>
                        <div class="plistrowbg"style="font-size: 26px;">10</div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow2"><div class="button_ok"><a href="#contact">Je choisis</a></div></div>
                    </li>
                    <li class="noselections">
                        <div class="plistrow1" style="color: #2DB573 !important">Premium</div>
                        <div class="plistrow2"><span class="h9" style="color: #245C5D !important;font-size: 20px;">*60€</span><br><span class="smallp">/mois</span></div>
                        <div class="plistrowbg"style="font-size: 26px;">50</div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow2"><div class="button_ok"><a href="#contact">Je choisis</a></div></div>
                    </li>
                    <li class="noselections">
                        <div class="plistrow1" style="color: #2DB573 !important">Ultimate</div>
                        <div class="plistrow2"><span class="h9" style="color: #245C5D !important;font-size: 20px;">*80€</span><br><span class="smallp">/mois</span></div>
                        <div class="plistrowbg" style="font-size: 26px;">illimité</div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow"><span class="ti-check cgreen"></span></div>
                        <div class="plistrow2"><div class="button_ok"><a href="#contact">Je choisis</a></div></div>
                    </li>
                </ul>
            </div>  
        </div>
    </div>
</section>

<!-- SECTION IMAGE -->
<section class="bg222 c999 relative z100 clearfix">
    <div class="cover1 col-xs-12 col-sm-12 col-md-6 h700" >&nbsp;</div>
    <div class="container cus-pos-abs">
        <div class="clearfix"></div>
        <div class="cover-right-text col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-6 sspacing">
            <h1 class="titlefont cwhite">Prise en charge de votre investissement</h1>
            <h3 class="titlefont mt20">
                Si vous répondez à ces critères, vous etes certainement concerné par une prise en charge :
            </h3>
            <div class="separator100 bg333 mtb30"></div>
            <ul class="aboutteamlist">
                <p class="size16">
                    Un audit pour adapter cet outil de relation client aux spécificités de votre métier peut être réalisé.<br><br> Nous pouvons vous proposer une formation prise en charge par votre OPCA pour mieux appréhender vos besoins et les solutions que peut vous apporter les solutions immatérielles tel que la géolocalisation ou l'archivage en ligne sécurisé.<br><br> Des développements spécifiques peuvent dans ce cadre y être associées. <br><br>Plus d'information : Jean Philippe Sourisce 02 41 34 83 74 .
                </p>
            </ul>			
        </div>
    </div>
</section>
<!-- END OF SECTION IMAGE -->

<div id="demo"class="bgmaincolordarken" style="background-color: #245C5D !important;">
    <div class="container ptb100 opensans cwhite size24 text-center">
        <span style="font-size: 45px">Découvrez</span> <span style="color:#2DB573;font-size: 45px">e-pilote</span> <span style="font-size: 45px">maintenant</span><br><br>
        <a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'presentation']); ?>" style="font-size: 25px" class="button_teste">Voir les vidéos</a>
        <img class="teste-img" src="<?= $this->Url->image('picto_demo.png', ['alt' => '']); ?>">
    </div>
</div>

<!-- SECTION CONTACT -->
<div class="" id="contact">
    <div class="container sspacing-title-button">
        <div class="row">
            <p class="text-center size30 mb10 pb20">Laissez-nous un message, nous vous rappelerons pour évoquer votre projet.</p>
            <p><?= $this->Flash->render(); ?></p>
            <?php echo $this->Form->create("Contact", ['id' => 'contact-form', 'type' => 'post']); ?>
            <div class="col-md-3">
                <input class="form-control formlarge" placeholder="Nom" name="nom" type="text" required="required">
                <input class="form-control formlarge mt17" placeholder="Email" name="email" type="text" required="required">
                <input class="form-control formlarge mt17" placeholder="Téléphone" name="telephone" type="text" required="required">
            </div>
            <div class="col-md-9">
                <textarea class="form-control formstyle" rows="7" name="message" placeholder="Message" required="required"></textarea>
            </div>
            <div class="clearfix"></div>
            <button style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-iteration-count: 2; animation-name: pulse;" type="submit" id="send-mail" class="btn btnwhitebig btn-default caps center mt30 wow pulse animated animated" data-wow-delay="0.4s" data-wow-duration="1s" data-wow-iteration="2"><i class="icon-mail"></i> Envoyer</button>
            <div class="clearfix"></div>
            <?php echo $this->Form->hidden("captcha"); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- END OF CONTACT -->

<?= $this->Html->scriptStart(); ?>
$(document).ready(function () {
//envoie mail
<!--$("#send-mail").click(function (e) {
var form = { };
$.each($("#contact-form").serializeArray(),function() {
form[this.name] = this.value;
});
$.ajax({
url: '<?php // echo \Cake\Routing\Router::url(array('controller' => 'Contacts', 'action' => 'ajaxSendMail')); ?>',
data: {
data: form
},
type: 'post',
success: function (html) {

}
});  
e.preventDefault();
});
});-->

<?= $this->Html->scriptEnd(); ?>
<?= $this->fetch('script'); ?>






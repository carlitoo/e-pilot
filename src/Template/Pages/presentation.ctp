<div class="bglight2">
    <div class="container pt60">

        <div id="sidecontent" class="col-md-9 offset-0 pr40 mb50">
            <!-- CONTENT -->

            <!-- post -->
            <div class="col-md-12">
<!--                <div class="relative" style="height: 400px;overflow: hidden">
                    <img src="img/e-pilote_presentation.jpg" class="fwi" alt="">                        
                </div>-->
                <div class="col-md-12 bgwhite pt30 pb50 plr50">
                    <div class="col-md-12 mt20">
                        <p class="fontproximabold size20">e-pilote : présentation du logiciel</p>
                        <span class="c666 lh24">
                            <iframe width="100%" height="400" src="https://www.youtube.com/embed/SjiRDrKccJE?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> 
                        </span>
                        <span class="c666 lh24">
                            Logiciel de gestion de la relation commerciale en ligne, e-pilote est un outil simple pour accompagner quotidiennement la gestion commerciale de votre entreprise. Comme il est simple, il est facile à faire évoluer pour répondre à vos attentes particulières : ajout de fonctionnalités (pilotage e-commerce, statistiques, objectif commerciaux, espace pro, gestion de la production, gestion des stocks et des fournisseurs...)
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>                
            </div>

            <!-- END CONTENT -->
        </div>
        <div class="col-md-3 offset-0">
            <div id="make-this-fixed-off" class="rightmenu ">
                <ul>
                    <li class="title">Choisissez une vidéo</li>
<!--                    <li><a href="#">les autres vidéos1<span></span></a></li>
                    <li><a href="#">les autres vidéos2<span></span></a></li>
                    <li><a href="#">les autres vidéos3<span></span></a></li>
                    <li><a href="#">les autres vidéos....<span></span></a></li>-->
                    <li><a href="#">bientôt disponible....<span></span></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- SECTION CONTACT -->
<div class="" id="contact">
    <div class="container sspacing-title-button">
        <div class="row">
            <p class="text-center size30 mb10 pb20">Laissez-nous un message, nous vous rappelerons pour évoquer votre projet.</p>
            <p><?= $this->Flash->render(); ?></p>
            <?php echo $this->Form->create("Contact", ['id' => 'contact-form', 'type' => 'post']); ?>
            <div class="col-md-3">
                <input class="form-control formlarge" placeholder="Nom" name="nom" type="text" required="required">
                <input class="form-control formlarge mt17" placeholder="Email" name="email" type="text" required="required">
                <input class="form-control formlarge mt17" placeholder="Téléphone" name="telephone" type="text" required="required">
            </div>
            <div class="col-md-9">
                <textarea class="form-control formstyle" rows="7" name="message" placeholder="Message" required="required"></textarea>
            </div>
            <div class="clearfix"></div>
            <button style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-iteration-count: 2; animation-name: pulse;" type="submit" id="send-mail" class="btn btnwhitebig btn-default caps center mt30 wow pulse animated animated" data-wow-delay="0.4s" data-wow-duration="1s" data-wow-iteration="2"><i class="icon-mail"></i> Envoyer</button>
            <div class="clearfix"></div>
            <?php echo $this->Form->hidden("captcha"); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- END OF CONTACT -->


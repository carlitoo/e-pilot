<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Form\ContactForm;

/**
 * Description of ContactController
 *
 * @author carlos-ignis
 */
class ContactsController extends AppController {

    /**
     * Validation ajax du formulaire de contact
     */
    public function ajaxSendMail() {        
        //desactivation de la vue
        $this->render(false);
        // Traitement du formulaire
        $contact = new ContactForm();

        if ($this->request->is('ajax') && isset($this->request->data['data']['captcha']) && empty($this->request->data['data']['captcha'])) {
            if ($contact->execute($this->request->data)) {
                $this->Flash->success("Votre message a bien été envoyé. Une copie vous a été transférée à l'adresse e-mail indiquée.");
            } else {
                $this->Flash->error("Il y a eu un problème lors de la soumission de votre formulaire.");
            }
        }
    }
}
    
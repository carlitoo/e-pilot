<?php
// Dans src/Form/ContactForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\Mailer\Email;

class ContactForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('nom', 'string')
            ->addField('email', ['type' => 'string'])
            ->addField('message', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('nom', 'length', [
                'rule' => ['minLength', 10],
                'message' => 'Un nom est requis'
            ])->add('email', 'format', [
                'rule' => 'email',
                'message' => 'Une adresse email valide est requise',
            ]);
    }

    protected function _execute(array $data){
        //envoie du mail
        $email = new Email('default');
        $email->viewVars(['data' => $data])
                ->template('contact')
                ->emailFormat('html')
                ->from(['ignis@ignis.fr' => 'E-pilote'])
                ->to('carlos.lusi@gmail.com')
                ->subject('Demande de contact')
                ->send('My message');
        
        return true;
    }
}
